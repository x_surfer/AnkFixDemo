
[andfix](https://github.com/alibaba/AndFix)

## 大概步骤

- 生成keystore 等东西，并在build.gradle 中指定地址以及密码。
- `./gradlew clean assembleRelease` 编译出一个apk 包，copy 出来作为 old 版本，
- 修改某个方法体中的代码，再编译出一个apk,包，copy 出来重命名为 new 的版本。
- 阿里的 apkpatch 工具加一系列参数 生成 我们要的.aptch 文件，更名为你预设的名字。
- 安装 old 版本到手机，作为有bug 的版本。
- `adb push <path>/out.apatch /<path>/out.apatch` ，其中两个路径前一个是当前out.apatch 文件；路径，后一个是代码中设置的路径。
- 在 app 中填写之前预设的名字，即可完成patch，不用重启app


## 注意

- 要给 app 读写 sdcard 的权限
- 同一个版本的patch 名字不能相同，否则不会重复add。
- 升级版本会导致所有补丁被删除
- 为了保证安全性在 patch 之前要先做 文件 MD5 的验证。关于签名 在 AndFix  中已经做了保证了
- 实验证明：AndFix 不支持添加方法，类，字段，能够add 进去，但是程序会挂
- 针对multi-dex 的app，如果在使用 akapatch 的时候，可能会报 `java.io.IOException: unable to open DEX file...` 的问题，那么参考这里 [multi-dex support](https://github.com/zhengyuqin/andfix_apkpatch_support_multidex)