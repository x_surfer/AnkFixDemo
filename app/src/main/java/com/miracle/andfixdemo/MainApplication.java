package com.miracle.andfixdemo;

import android.app.Application;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.alipay.euler.andfix.patch.PatchManager;

import java.io.IOException;

/**
 * @author david
 */
public class MainApplication extends Application {

    private static final String TAG = "david";

    /**
     * patch manager
     */
    private PatchManager mPatchManager;

    @Override
    public void onCreate() {
        super.onCreate();
        // initialize
        mPatchManager = new PatchManager(this);
        mPatchManager.init("1.0");
        Log.d(TAG, "init.");

        // load patch
        mPatchManager.loadPatch();
        Log.d(TAG, "apatch loaded.");

//        addPatch();

    }

    public void addPatch(String name) {
        try {
            // .apatch file path
            String patchFileString = Environment.getExternalStorageDirectory()
                .getAbsolutePath() + "/" + name + ".apatch";
            mPatchManager.addPatch(patchFileString);
            Toast.makeText(this, "apatch:" + patchFileString + " added.", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            Log.e(TAG, "", e);
            Toast.makeText(this, "add failed :" + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

}
